# PAF TME 8 : Générateurs monadiques avec QuickCheck

L'énoncé de ce TME se trouve dans le fichier
`src/PAF_TME8_GenMonad.hs` qu'il faut compléter.

Des versions markdown et pdf de l'énoncé sont également disponible,
pour un meilleur confort de lecture.


Bon travail !

----
Copyright (C) 2020 - Frédéric Peschanski



